/*
 * 参数说明
 * isView     : Boolean //是否为视图接口
 * isGetCache : Boolean //是否使用缓存
 * isCache    : Boolean //是否缓存
 */

exports.API = {

   //首页
   "/index" : {
       type         : 'BUSSINESS',
       method       : "GET",
       isView       : true,
       route        : "/ed/index"
   },

   //动态详情页
   "/dynamicDetail" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/ed/dynamicDetail"
    },

   //产品中心
   "/product" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/ed/product"
    },

    //更多版本
    "/moreVersion" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/ed/moreVersion"
    },

    //解决方案
    "/solution" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/ed/solution"
    },

    //开发者中心
    "/developerCenter" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/ed/developerCenter"
    },

    //案例
    "/case" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/ed/case"
    },

    //社区
    "/community" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/ed/community"
    },

    //关于我们
    "/about" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/ed/about"
    },

    //教程
    "/tutorial" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/ed/tutorial"
    },

    //api文档信息
    "/apiDetail" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/ed/apiDetail"
    },

    //注册
    "/register" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/ed/register"
    },

    //生成验证码接口
    "/common/createCode" : {
        type         : 'BUSSINESS',
        method       : "GET"
    },

    //注册处理
    "/user/register" : {
        type         : 'BUSSINESS',
        method       : "POST"
    },

    //登录
    "/login" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/ed/login"
    },

    //登录处理
    "/user/login" : {
        type         : 'BUSSINESS',
        method       : "POST"
    },

    //常见问题
    "/problems" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/ed/problems"
    },

    //新增常见问题
    "/user/addQuestion" : {
        type         : 'BUSSINESS',
        method       : "POST"
    },

    //查询常见问题
    "/user/searchQuestion" : {
        type         : 'BUSSINESS',
        method       : "GET"
    },

    //登录处理
    "/common/getTSTToken" : {
        type         : 'BUSSINESS',
        method       : "POST"
    },

    //会员中心
    "/userCenter" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/ed/userCenter"
    },

    //更新用户基本信息
    "/user/updateUserInfo" : {
        type         : 'BUSSINESS',
        method       : "POST"
    },

    //删除用户问题（逻辑删除）
    "/user/deleteUserQuestion" : {
        type         : 'BUSSINESS',
        method       : "POST"
    },

    //标记问题为已解决
    "/user/overUserQuestion" : {
        type         : 'BUSSINESS',
        method       : "POST"
    },

    //捐赠
    "/donation" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/ed/donation"
    },

    //捐赠
    "/user/addDonation" : {
        type         : 'BUSSINESS',
        method       : "POST"
    },

    //维护人员详情页
    "/userDetail" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/ed/userDetail"
    },

    //申请成为Easy-D维护人员
    "/applyMaintainUser" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/ed/applyMaintainUser"
    },

    //申请Easy-D开发维护人员
    "/user/applyMaintainUser" : {
        type         : 'BUSSINESS',
        method       : "POST"
    },

    //下载
    "/download" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/ed/download"
    },

    //功能征集
    "/functionVote" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/ed/functionVote"
    },

    //技术文章
    "/technologyArticle" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/ed/technologyArticle"
    },

    //技术文章详情
    "/technologyArticleDetail" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/ed/technologyArticleDetail"
    },
    

    //新增投票功能
    "/user/addFunctionVote" : {
        type         : 'BUSSINESS',
        method       : "POST"
    },

    //查询投票功能
    "/user/searchFunctionVote" : {
        type         : 'BUSSINESS',
        method       : "GET"
    },

    //追加投票
    "/user/additionalFunctionVote" : {
        type         : 'BUSSINESS',
        method       : "POST"
    },

    // OA相关界面
    "/oa/introduce" : {
        type         : 'BUSSINESS',
        method       : "GET",
        isView       : true,
        route        : "/oa/introduce"
    },
    
}