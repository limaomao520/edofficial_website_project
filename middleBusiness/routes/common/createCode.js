exports.createCode = function (req, res) {

    var svgCaptcha = require('svg-captcha');
    var captcha = svgCaptcha.create();
    req.session.captcha = captcha.text;
    res.type('svg');

    //将验证码结果存放在SESSION中
    req.session.code = captcha.text;
    res.status(200).send(captcha.data);
   
}