exports.getTSTToken = function(req,res) {

    var OSS = require('ali-oss');
    var STS = OSS.STS;
    var co = require('co');
    var fs = require('fs');
    
    var filePath = "./aliyun_cache_data/tst_token.txt";

    var sts = new STS({
        accessKeyId: 'LTAIOPIeCBbjnY4y',
        accessKeySecret: 'AatFgnzrOrRRhlB7IlhL9j0vsVIS4p'
    });
    
    var resultData = {};
	
	var tstToken = "";

	//任务流创建
    async.waterfall([
    	
    	//判断token文件是否存在
        function(cb){

            fs.exists(filePath, function(exists) {  

                //如果文件不存在,则进行token的获取并且创建文件
                if(!exists){

                   
                    cb();

                //文件存在，查看文件内容是否快过期    
                }else{

                    //获取文件修改日期
                    fs.stat(filePath,function(err,data){
                        
                        //上次修改时间戳
                        var updateTime = (Date.parse(data.mtime)/1000);

                        //获取当前时间戳
                        var nowTime    = common.nowTime(false);

                        //查看离7200过期还剩多久
                        var tempTime   = nowTime - updateTime;

                        if(tempTime > 3000){

                            cb();

                        //直接读取文件的内容返回，说明token还未过期    
                        }else{

                            fs.readFile(filePath, function (err, data) {

                                if (err) {
                                	
                                    cb();
                                }

                                //获取文件保存的token信息
                                tstToken = data.toString();

								var tstTokenArr = tstToken.split("|");
							
                                resultData.AccessKeyId 	   = tstTokenArr[0];
                                resultData.AccessKeySecret = tstTokenArr[1];
                                resultData.SecurityToken   = tstTokenArr[2];

                                RES.successResponse(res,resultData);

                            });
                        }

                    })
                } 
            });  
        },
        
        
        //请求token信息
        function(cb){

            co(function* () {
               
                var token = yield sts.assumeRole('acs:ram::1329968499900995:role/readtestappreadonly','','3600','ds');
            
        		//判断是否请求失败
                resultData = token.credentials;
        			
        		cb();
        		
     		}).catch(function (err) {

                return RES.errorResponse(res, false, "TST验权失败");

     		});

        },

    	//作为文件缓存，写入本地文件
        function(cb){
        	
        	var tstStr = resultData.AccessKeyId + "|" + resultData.AccessKeySecret + "|" + resultData.SecurityToken;

            fs.writeFile(filePath,tstStr,function(err) {

                if(err) {
                    return RES.errorResponse(res, false, "阿里云OSS临时令牌获取失败");
                }

                cb();

            });

        }
    	
    	
    ],function(err, result){
        
       //返回最终拼接结果
       RES.successResponse(res,resultData);
    
    });


    

}