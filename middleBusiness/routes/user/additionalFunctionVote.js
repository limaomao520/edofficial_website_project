exports.additionalFunctionVote = function (req, res) {

    var bodyParams = req.routeInfo.bodyParams;

  
    //任务流创建
    async.waterfall([

        //查询该用户是否已追加过该投票了
        function(cb){
     
            db.where({

                "fvr_fvid" : bodyParams.fv_id,
                "fvr_uid"  : bodyParams.u_id

            }).select("ed_function_vote_record",function(rs){

                if(rs == 'ERROR'){
                    return RES.errorResponse(res, false, "抱歉，追加投票失败");
                }

                if(rs.length > 0){
                    return RES.errorResponse(res, false, "抱歉，您已经对该项投过票了");
                }

                cb()
                
            })

        },

        //更新投票数
        function(cb){

            db.where({
                "fv_id" : bodyParams.fv_id
            }).updateInc("fv_vote_count","ed_function_vote",function(rs){

                if(rs == 'ERROR'){
                    return RES.errorResponse(res, false, "抱歉，追加投票失败");
                }

                cb()
                
            })

        },

        //新增投票记录
        function(cb){

            db.add({
                "fvr_fvid"        : bodyParams.fv_id,
                "fvr_uid"         : bodyParams.u_id,
                "fvr_create_time" : common.nowTime()
            },"ed_function_vote_record",function(rs){

                if(rs == 'ERROR'){
                    return RES.errorResponse(res, false, "抱歉，追加投票失败");
                }else{

                    cb();

                }

            })

        }

    ], function (err, result) {

        //返回最终拼接结果
        RES.successResponse(res);

    });


}