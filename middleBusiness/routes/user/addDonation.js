exports.addDonation = function (req, res) {

    var bodyParams = req.routeInfo.bodyParams;

    //验证验证码是否正确
    if(bodyParams.code != req.session.code){
        return RES.errorResponse(res, false, "抱歉，验证码错误");
    }

    //任务流创建
    async.waterfall([

        //新增问题
        function(cb){

            db.add({

                "d_name"        : bodyParams.ed_name,
                "d_money"       : bodyParams.ed_money,
                "d_desc"        : bodyParams.ed_desc,
                "d_create_time" : common.nowTime()

            },"ed_donation",function(rs){

                if(rs == 'ERROR'){
                    return RES.errorResponse(res, false, "抱歉，捐赠信息提交失败");
                }else{
                    cb();
                }

            })
        }

    ], function (err, result) {

        //返回最终拼接结果
        RES.successResponse(res);

    });


}