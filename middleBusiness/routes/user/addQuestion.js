exports.addQuestion = function (req, res) {

    var bodyParams = req.routeInfo.bodyParams;

    //验证验证码是否正确
    if(bodyParams.q_code != req.session.code){
        return RES.errorResponse(res, false, "抱歉，验证码错误");
    }

    //任务流创建
    async.waterfall([

        //新增问题
        function(cb){

            db.add({
                "q_uid"         : bodyParams.q_uid,
                "q_title"       : bodyParams.q_title,
                "q_desc"        : bodyParams.q_desc,
                "q_images"      : bodyParams.q_images,
                "q_create_time" : common.nowTime()
            },"ed_question",function(rs){

                if(rs == 'ERROR'){
                    return RES.errorResponse(res, false, "抱歉，问题提交失败");
                }else{

                    cb();

                }

            })


        }

    ], function (err, result) {

        //返回最终拼接结果
        RES.successResponse(res);

    });


}