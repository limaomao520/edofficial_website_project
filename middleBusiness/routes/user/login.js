exports.login = function (req, res) {

    var bodyParams = req.routeInfo.bodyParams;

    var resultData = {};

    //任务流创建
    async.waterfall([

       
        //查询是否已注册过该帐号
        function(cb){

            db.where({"u_username":bodyParams.username,"u_password":bodyParams.password}).select("ed_user",function(rs){

                if(rs == 'ERROR'){
                    return RES.errorResponse(res, false, "抱歉，用户登录失败");
                }

                if(rs.length <= 0){
                    return RES.errorResponse(res, false, "抱歉，帐号或密码不正确");
                }

                var userInfo = rs[0];

                //保存在SESSION中
                req.session.userid = userInfo.u_id;

                resultData.userid   = userInfo.u_id;
                resultData.username = userInfo.u_username;
                resultData.nickname = userInfo.u_nickname;
                resultData.header   = userInfo.u_header;

                cb();

            });

        }

    ], function (err, result) {

        //返回最终拼接结果
        RES.successResponse(res,resultData);

    });


}