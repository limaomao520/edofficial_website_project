exports.register = function (req, res) {

    var bodyParams = req.routeInfo.bodyParams;

    var resultData = {};

    
    //任务流创建
    async.waterfall([

        //验证验证码是否正确
        function(cb){

            //验证验证码是否正确
            if(bodyParams.code != req.session.code){
                return RES.errorResponse(res, false, "抱歉，验证码错误");
            }

            cb();

        },

        //查询是否已注册过该帐号
        function(cb){

            db.where({"u_username":bodyParams.username,}).select("ed_user",function(rs){

                if(rs == 'ERROR'){
                    return RES.errorResponse(res, false, "抱歉，用户注册失败");
                }

                if(rs.length > 0){
                    return RES.errorResponse(res, false, "抱歉，该帐号已被注册");
                }
                
                cb();

            });

        },

        //注册操作
        function(cb){

            db.add({
                "u_username"    : bodyParams.username,
                "u_password"    : bodyParams.password,
                "u_nickname"    : bodyParams.nickname,
                "u_header"      : bodyParams.headerData,
                "u_activate"    : "1",
                "u_create_time" : common.nowTime()
            },"ed_user",function(rs){

                if(rs == 'ERROR'){
                    return RES.errorResponse(res, false, "抱歉，用户注册失败");
                }else{

                    resultData.userid   = rs;
                    resultData.username = bodyParams.username;
                    resultData.nickname = bodyParams.nickname;
                    resultData.header   = bodyParams.headerData;

                    cb();

                }

            })

        }


    ], function (err, result) {

        //返回最终拼接结果
        RES.successResponse(res,resultData);

    });

   
}