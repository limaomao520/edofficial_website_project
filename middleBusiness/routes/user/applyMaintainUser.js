exports.applyMaintainUser = function (req, res) {

    var bodyParams = req.routeInfo.bodyParams;

    //验证验证码是否正确
    if(bodyParams.code != req.session.code){
        return RES.errorResponse(res, false, "抱歉，验证码错误");
    }

    //任务流创建
    async.waterfall([

        //新增问题
        function(cb){

            db.add({
                "am_name"        : bodyParams.am_name,
                "am_qq"          : bodyParams.am_qq,
                "am_weixin"      : bodyParams.am_weixin,
                "am_create_time" : common.nowTime()
            },"ed_apply_maintain",function(rs){

                if(rs == 'ERROR'){
                    return RES.errorResponse(res, false, "抱歉，申请开发维护者失败");
                }else{

                    cb();

                }

            })

        }

    ], function (err, result) {

        //返回最终拼接结果
        RES.successResponse(res);

    });


}