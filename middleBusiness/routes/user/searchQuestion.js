exports.searchQuestion = function (req, res) {

    var restfulParams = req.routeInfo.restfulParams;

    var where = {};

    if(restfulParams.u_id != undefined){
        where.u_id = restfulParams.u_id;
    }

    where.q_is_delete = 0;

    //查询问题列表
    db.where(where).order("q_create_time desc").limit(restfulParams.skip,restfulParams.limit).select("v_ed_question_user_info",function(rs){

        if(rs == 'ERROR'){
           console.log("数据查询失败");  
        }

        //如果登录了，进行标记判断
        if(req.session.userid){

            //新增是否出现标记为已解决按钮
            for(var i=0;i<rs.length;i++){
                if(rs[i].q_uid == req.session.userid && rs[i].q_status == 0){
                    rs[i].isTagSolve = 1;
                }else{
                    rs[i].isTagSolve = 0;
                }
            }

        }

        RES.successResponse(res,rs);

    })

}