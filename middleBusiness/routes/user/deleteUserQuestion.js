exports.deleteUserQuestion = function (req, res) {

    var bodyParams = req.routeInfo.bodyParams;


    //查询问题列表
    db.where({q_id:bodyParams.q_id}).update({"q_is_delete":1},"ed_question",function(rs){

        if(rs == 'ERROR'){
           return RES.errorResponse(res, false, "抱歉，问题删除失败");
        }

        RES.successResponse(res);

    })

}