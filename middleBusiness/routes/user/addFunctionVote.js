exports.addFunctionVote = function (req, res) {

    var bodyParams = req.routeInfo.bodyParams;

    //验证验证码是否正确
    if(bodyParams.v_code != req.session.code){
        return RES.errorResponse(res, false, "抱歉，验证码错误");
    }

    //任务流创建
    async.waterfall([

        //新增问题
        function(cb){

            db.add({
                "fv_name"        : bodyParams.fv_name,
                "fv_create_time" : common.nowTime()
            },"ed_function_vote",function(rs){

                if(rs == 'ERROR'){
                    return RES.errorResponse(res, false, "抱歉，功能需求提交失败");
                }else{

                    cb();

                }

            })


        }

    ], function (err, result) {

        //返回最终拼接结果
        RES.successResponse(res);

    });


}