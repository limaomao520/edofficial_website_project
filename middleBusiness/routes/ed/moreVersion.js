exports.moreVersion = function (req, res) {

    var type = req.query.type;

    var versionTitle = "";
    var versionData  = [];

    if(type == 'atomic'){

        versionTitle = "原子层开发框架";
        versionData  = [
            { id : 1, text : "项目全局文件功能" },
            { id : 2, text : "全局常量功能配置" },
            { id : 3, text : "接口白名单配置功能" },
            { id : 4, text : "参数自动验证" },
            { id : 5, text : "接口声明功能" },
            { id : 6, text : "接口缓存配置功能" },
            { id : 7, text : "接口自动构建功能" },
            { id : 8, text : "URL路由多功能查询" },
            { id : 9, text : "数据库操作类库,支持SQL链式调用" },
            { id : 10, text : "聚合层验权功能" },
            { id : 11, text : "接口初始化功能" },
            { id : 10, text : "系统接口配置和业务接口配置分离" },
            { id : 12, text : "框架底层路由功能实现" },
            { id : 13, text : "自定义接口参数验证" }
        ];

    }else if(type == 'aggregation'){

        versionTitle = "聚合层开发框架";
        versionData  = [
            { id : 1, text : "项目全局文件功能" },
            { id : 2, text : "核心层连接配置" },
            { id : 3, text : "数据库直连功能" },
            { id : 4, text : "单服务集群启动配置" },
            { id : 5, text : "接口缓存配置功能" },
            { id : 6, text : "权限开关配置" },
            { id : 7, text : "验权服务功能" },
            { id : 8, text : "任务流方法封装" },
            { id : 9, text : "框架路由处理功能" }
        ];

    }else if(type == 'admin'){

        versionTitle = "管理端开发框架";
        versionData  = [
            { id : 1, text : "项目打包功能" },
            { id : 2, text : "内置系统管理功能" },
            { id : 3, text : "内置运维管理功能" },
            { id : 4, text : "内置接口文档管理功能" },
            { id : 5, text : "内置开发管理功能" },
            { id : 6, text : "提供常用开发封装组件" },
            { id : 7, text : "内置权限功能" },
            { id : 8, text : "vue+webpack标注组合框架搭建" },
            { id : 9, text : "内置安装常用三方组件库" }
        ];

    }else if(type == 'pc'){

        versionTitle = "PC端开发框架";

    }else if(type == 'weixin'){

        versionTitle = "微信端开发框架";

    }else if(type == 'service'){

        versionTitle = "服务层开发框架";
        versionData  = [
            { id : 1, text : "项目全局文件功能" },
            { id : 2, text : "全局常量功能配置" },
            { id : 3, text : "参数自动验证" },
            { id : 4, text : "接口声明功能" },
            { id : 5, text : "接口缓存配置功能" },
            { id : 6, text : "数据库操作类库,支持SQL链式调用" },
            { id : 7, text : "接口初始化功能" },
            { id : 8, text : "系统接口配置和业务接口配置分离" },
            { id : 9, text : "框架底层路由功能实现" },
            { id : 10, text : "自定义接口参数验证" }
        ];

    }

    Render.viewRender(req,res,'moreVersion',{type:type,title:"更多版本",versionTitle:versionTitle,versionData:versionData})

}