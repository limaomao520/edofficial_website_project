exports.problems = function (req, res) {

    console.log("------");
    console.log(req.session.userid);

    //判断是否有分页参数，如果没有则表示默认显示第一页
    var skip;
    var limit = 20;
    if(req.query.page  == undefined || req.query.page == 0){
        skip  = 0;
    }else{
        skip = req.query.page * limit;
    }


    R.SEND_HTTP(
        req,
        res,
        {
            url: 'http://127.0.0.1:8002/user/searchQuestion/skip/' + skip + "/limit/" + limit,
            method : 'GET'
        },
        function (data) {

            // //如果登录了，进行标记判断
            // if(req.session.userid){

            //     //新增是否出现标记为已解决按钮
            //     for(var i=0;i<data.result.length;i++){
            //         if(data.result[i].q_uid == req.session.userid && data.result[i].q_status == 0){
            //             data.result[i].isTagSolve = 1;
            //         }else{
            //             data.result[i].isTagSolve = 0;
            //         }
            //     }

            // }

            Render.viewRender(req,res,"problems",{title : "常见问题", listData : data.result })

        
        }
    )

   
    // //查询问题列表
    // db.order("u_create_time desc").limit(skip,limit).select("v_ed_question_user_info",function(rs){

    //     if(rs == 'ERROR'){
    //        console.log("数据查询失败");  
    //     }

    //     //如果登录了，进行标记判断
    //     if(req.session.userid){

    //         //新增是否出现标记为已解决按钮
    //         for(var i=0;i<rs.length;i++){
    //             if(rs[i].q_uid == req.session.userid && rs[i].q_status == 0){
    //                 rs[i].isTagSolve = 1;
    //             }else{
    //                 rs[i].isTagSolve = 0;
    //             }
    //         }

    //     }

    //     Render.viewRender(req,res,"problems",{title : "常见问题", listData : rs})

    // })

}