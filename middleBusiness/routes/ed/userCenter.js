exports.userCenter = function (req, res) {

    //获取用户信息
    console.log(req.session.userid);
    if(req.session.userid == undefined){
        Render.viewRender(req,res,"login",{title : "登录"})
        return;
    }

    //获取用户信息
    db.where({u_id:req.session.userid}).select("ed_user",function(rs){

        if(rs == 'ERROR'){
            console.log("用户信息获取失败");
        }

        var userInfo = rs[0];

        Render.viewRender(req,res,'userCenter',{title:"会员中心",userData : userInfo})

    });

    

}