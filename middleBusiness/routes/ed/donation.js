exports.donation = function (req, res) {

    //获取捐赠信息
    db.where({d_is_confirm:1}).order("d_create_time desc").select("ed_donation",function(rs){

        if(rs == 'ERROR'){
            console.log("捐赠信息获取失败");
        }

        //计算总捐助额
        var moneyCount = 0;
        for(var i =0;i<rs.length;i++){
            moneyCount += rs[i].d_money;
        }

        Render.viewRender(req,res,'donation',{title:"捐赠我们",donationData:rs,moneyCount:moneyCount})

    });

    

}