exports.functionVote = function (req, res) {

    //查询投票信息
    var where = {};

    where.fv_is_delete = 0;
    where.fv_is_use    = 1;
    where.fv_status    = 0;

    //查询问题列表
    db.where(where).order("fv_vote_count desc").limit(0,1000).select("ed_function_vote",function(rs){

        if(rs == 'ERROR'){
           console.log("数据查询失败");  
        }

        //获取总票数
        var voteCount = 0;

        for(var i=0;i<rs.length;i++){
            voteCount += rs[i].fv_vote_count;
        }

        //计算投票的百分占比
        for(var i=0;i<rs.length;i++){

           var percentage = ((rs[i].fv_vote_count / voteCount)*100).toFixed(2);
           rs[i].percentage = percentage;
    
        }


        Render.viewRender(req,res,'functionVote',{title:"功能投票",voteData:rs})

    })


    
}