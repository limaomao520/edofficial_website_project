exports.apiDetail = function (req, res) {

    //根据类型获取标题
    var apiType = req.query.type;

    var typeTitle = "";
    var typePath  = ""

    //原子层相关文档标题，地址
    if(apiType == 'atomic'){

        typeTitle = "原子层框架";
        typePath  = "/md/api/atomic";

    }else if(apiType == 'aggregation'){

        typeTitle = "聚合层框架";
        typePath  = "/md/api/aggregation";

    }else if(apiType == 'admin'){

        typeTitle = "后台管理端框架";
        typePath  = "/md/api/admin";

    }else if(apiType == 'pc'){

        typeTitle = "PC客户端框架";
        typePath  = "/md/api/pc";

    }else if(apiType == 'weixin'){

        typeTitle = "微信客户端框架";
        typePath  = "/md/api/weixin";

    }else if(apiType == 'file_service'){

        typeTitle = "文件OSS服务";
        typePath  = "/md/api/file_service";

    }else if(apiType == 'sms_service'){
    
        typeTitle = "短信封装服务";
        typePath  = "/md/api/sms_service";

    }else if(apiType == 'im_service'){

        typeTitle = "即时通讯服务";
        typePath  = "/md/api/im_service";

    }else if(apiType == 'service'){

        typeTitle = "服务层框架";
        typePath  = "/md/api/service";

    }else if(apiType == 'news_project'){

        typeTitle = "项目实战-文章管理系统实现";
        typePath  = "/md/api/news_project";

    }
    

    Render.viewRender(req,res,'apiDetail',{
        title     : "api文档",
        apiType   : apiType,
        typeTitle : typeTitle,
        typePath  : typePath
    })

}