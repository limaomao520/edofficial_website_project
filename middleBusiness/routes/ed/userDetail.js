exports.userDetail = function (req, res) {

    var userId = req.query.id;

    var resultData = {};

    //查询维护人员基本信息
    db.where({au_id:userId}).select("ed_maintain_user",function(rs){

        if(rs == 'ERROR'){
            console.log("用户信息获取失败");
        }

        resultData.userInfo = rs[0];

        //获取身份数组
        resultData.userInfo.au_identity = rs[0].au_identity.split("|");

        //查询该用户所维护的产品
        db.where({p_muid:userId,p_is_delete:0}).select("ed_product",function(rs){

            if(rs == 'ERROR'){
                console.log("用户维护产品信息获取失败");
            }

            resultData.productData = rs;

            console.log(resultData)

            Render.viewRender(req,res,'userDetail',{title:"维护人员详情",resultData:resultData})


        });

    });


}