/****************************
 * 配置文件
 ****************************/

exports.configInfo = {

  /*** 核心服务器信息 ***/
  core_server : {
    port        : 80,                      //端口号
    name        : "atomic-layer-php-v1.0", //服务器名称
    url         : "http://127.0.0.1/"      //核心服务器地址
  },

  /*** 服务器配置 ***/
  server : {
    ip           : "http://127.0.0.1",               //Ip地址
    port         : 8002,                             //端口号
    name         : "",                               //服务器名称  
    crossOrigin  : true,                             //是否接收跨域请求
    businessPath : "middleBusiness"                  //中间层业务目录
  },

  /*** mysql配置 ***/
  mysql : {
    host      : "rm-2ze1kg3ip763wer90o.mysql.rds.aliyuncs.com",
    user     	: "liweixuan",
    password 	: "12345678",
    port			: 3306,
    database	: "ed"
  },

  /*** 日志配置 ***/
  log4 : {
    appenders : [
      {
        type       : "console"
      },
      {
        type       : "dateFile",           //文件输出
        filename   : "logs/system.log",    //日志存放位置
        maxLogSize : 1024,                 //文件大小上限
        backups    : 3                     //日志备份
      }
    ],
    replaceConsole: true
  },

  /*** 服务器基本配置 ***/
  base : {
    isCrossOrigin     : true,         //是否开启跨域处理
    isClusterStart    : false,        //是否以集群方式启动
  },

  /*** 权限相关配置 ***/
  permissionConfig : {
    isUserPermission       : false, //是否开启用户权限验证
    isOpenActionPermission : false, //是否开启操作级权限验证
    permissionWhiteList : [     //权限验证白名单，不需要进行验证的接口
      
    ]
  },

  /*** 服务器验权相关 ***/
  serverAuth : {
    serverNo : "S0009"  //服务器编号
  },

  /*** 公共参数配置 ***/
  publicConfig : {
    LIMIT : 20     //默认每页显示数据量
  },

  /*** 自定义Header头Key ***/
  headersConfig : ",Is-Admin,Action-Ids"

};