/****************************
 * 常用基本－网络请求公共方法
 ****************************/

/**
 * GET请求共通方法
 **/
var qs = require('qs')

exports.SEND_HTTP = function(req,res,options,cb){

	//最终请求参数
	var httpOptions = {};

	//判断是否有RESTFUL参数
	var restfulParams = "";
	if(!(common.isNullObj(options.params))){
		  restfulParams = common.JsonTransformRestFulParams(options.params);
	}

	//设置请求地址
	httpOptions.url = options.url + restfulParams;

	//设置请求方式
	httpOptions.method = options.method == undefined ? "GET" : options.method;

	//设置BODY参数
	httpOptions.form = options.data == undefined ? {} : qs.stringify(options.data);

	//设置压缩
	httpOptions.gzip = true;

	//设置返回值类型
	httpOptions.json = true;

	//获取聚合层服务器编号
	var serverNo = config.serverAuth.serverNo;

	//设置header头
	httpOptions.headers = {
		"Content-Type": 'application/x-www-form-urlencoded',
		"server-No"   : serverNo
	};

	//发起HTTP请求
	request(httpOptions,function(error,response,body){

			//判断是否请求有错误
			if(error){
				return RES.errorResponse(res, false, MESSAGE.apiError.noNetWork);
			}

			//判断是否有业务错误
			if(!error && response.statusCode == 200){

				//返回业务错误信息
				if(body.success){
					cb(body);
				}else{
					return RES.response(res, false, body);
				}	

			}

	});

};
