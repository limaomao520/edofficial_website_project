exports.response = function(res,isSuccess,data){
	res.send(data);
};

exports.successResponse = function(res,data){
	var resultData = {};
	resultData.success = true;
	resultData.result  = data;
	res.send(resultData);
};

exports.errorResponse = function(res,isSuccess,data){
	res.send({ 
		success : isSuccess,
		result  : { 
	       code : 'bussiness_error' , message : data 
		} 
	});
};