exports.viewRender = function(req,res,tmplate,data){

    var fs = require('fs');

    //检测是否有缓存文件
    var htmlName = req.routeInfo.secondMenuLevelName;

    //判断是否有参数信息
    var params = req.query;

    //参数文件名
    var paramsStr = "";

    //如果有参数信息，则将参数带上生成缓存文件名
    if(params != undefined){
        for(var k in params){
            paramsStr += "_" + k + "_" + params[k];
        }
    }

    if(paramsStr == ""){
        htmlName = htmlName + ".html"
    }else{
        htmlName = htmlName + paramsStr + ".html"
    }

    fs.exists('./public/html/' + htmlName, function(exists) {

        //判断是否读取缓存
        if(req.routeInfo.isGetCache){

            if(exists) {

                console.log("读取了缓存文件")

                res.redirect('http://127.0.0.1:'+config.server.port+'/html/' + htmlName);

            }else{

                res.render(tmplate, data , function(err, html){

                    if(err){
                        return res.render('error',{error:err},function(err,html){
                            res.send(html);
                        });
                    }

                    //判断是否缓存该文件
                    if(req.routeInfo.isCache){
            
                        fs.writeFile('./public/html/' + htmlName ,html,function(err) {
            
                            if (err) {
                                console.log("静态文件生成失败")
                            }
            
                            res.send(html)
                            
                        });
                    
                    }else{

                        res.send(html)

                    }

                });

            }

        }else{

            res.render(tmplate, data , function(err, html){

                if(err){
                    return res.render('error',{error:err},function(err,html){
                        res.send(html);
                    });
                }


                //判断是否缓存该文件
                if(req.routeInfo.isCache){

                    console.log("缓存该文件")
        
                    fs.writeFile('./public/html/' + htmlName ,html,function(err) {
        
                        if (err) {
                            console.log("静态文件生成失败")
                        }
        
                        res.send(html)
                        
                    });
                }else{

                    res.send(html)

                }

            });
        }
    });

}