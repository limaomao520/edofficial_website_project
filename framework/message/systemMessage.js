exports.apiError = {
	noRegister 		 : '该接口不存在',
	noPage 		     : '该页面不存在',
	noMethod   	     : '不支持的请求方式',
	noRestful  		 : 'RESTFUL参数格式不正确',
	noVersion  		 : '缺少必传参数版本号',
	noVersionFormat  : '版本号格式错误，请参考文档',
	noNetWork  	     : '网络请求失败'
};
