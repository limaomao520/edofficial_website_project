//本地数据管理
var USER = {};

//判断是否登录
USER.isLogin = function(){

    if(!customStorage.getPersistenceValue("userId")){
        return false
    }else{
        return true
    }

}

//获取登录用户的ID
USER.getUserId = function(){

    if(!customStorage.getPersistenceValue("userId")){
        return 0
    }else{
        return customStorage.getPersistenceValue("userId")
    }

}

window.USER = USER;