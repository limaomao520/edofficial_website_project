
	// HTTP 请求封装
	var HTTP = {};

	/*
	 * get请求
	 * @params {url} 请求地址
	 * @params {data} 请求参数
	 * @params {callback} 回调函数
	 */
	HTTP.get = function(url, data, callback) {
				var $data = typeof data === 'function' ? null : data;
				var $callback = typeof data === 'function' ? data : callback;
				send('get', url, $data, $callback);
	};

	/*
	 * post请求
	 * @params {url} 请求地址
	 * @params {data} 请求参数
	 * @params {callback} 回调函数
	 */
	HTTP.post = function (url, data, callback) {
			send('post', url, data, callback);
		};

	//将工具添加至window对象
	window.HTTP = HTTP;


	/*
	 * 请求发送方法
	 * @params {$method} 请求方式
	 * @params {$url} 请求地址
	 * @params {$data} 请求参数
	 * @params {$callback} 回调函数
	 */
	var send = function ($method, $url, $data, $callback) {

		//回调函数定义
		var _callback = function (e, data) {

			// 回调
			if (typeof $callback === 'function') {
				$callback(e, data);
			} else {
				console.log("错误请求");
			}
		};


		//查看是否为POST请求,如果是则取出token并设置在头信息中
		if($method == 'post'){

			// if(_USER.isLogin()){

			// 	//取出本地token信息
			// 	var userToken = _USER.getUserToken();
			
			// 	//设置在ajax的请求头中
			// 	$.ajaxSetup({
			// 		headers: {'usertoken': userToken}
			//     });

			// }
		}

		// 设置异步请求选项
		var options = {
			url			: $url,
			method	: $method,
			data		: $data,
			dataType: "json",
			jsonp		: true,
			timeout	: 150000,  //设置超时时间15秒
			success	: function (data) {

				console.log(data);

				//判断业务是否成功
				if (data.success) {
					_callback(data.result);
				} else {

					POP.endLoading();
					POP.error("错误提示",data.result.message);
					return;

					// _callback(true, data.result.message);
				}
			},
			error: function (e) {
				_callback(e);
			}
		};

		// 发送异步请求
		$.ajax(options);

	};


	// 弹出框共通封装
	var POP = {};

	PNotify.prototype.options.styling = "fontawesome";

	//信息弹出框，不带按钮
	POP.alert = function(title,content){

		var $title   = '提示';
		var $content = '';

		if(arguments.length == 2){
			$title   = arguments[0] == 'undefined' ? '' : arguments[0];
			$content = arguments[1] == 'undefined' ? '' : arguments[1];
		}else{
			$content = arguments[0];
		}

		var alertHTML = $('<div class="modal fade" id="alertPOP" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">' +
			'<div class="modal-dialog">' +
			'<div class="modal-content">' +
			'<div class="modal-header">' +
			'<h4 class="modal-title">'+$title+'</h4>' +
			'</div>' +
			'<div class="modal-body">' + $content +
			'</div></div></div></div>');
		$("body").append(alertHTML);
		alertHTML.modal('show');

		alertHTML.on('hidden.bs.modal', function (e) {
			alertHTML.remove();
		});

	};

	//信息弹出框带确认与取消按钮,小确认框
	POP.confirm = function(title,content,okCallBack){

		var alertHTML = $('<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">' +
			'<div class="modal-dialog modal-sm">' +
			'<div class="modal-content">' +
			'<div class="modal-header">' +
			'<h4 class="modal-title">'+title+'</h4>' +
			'</div>' +
			'<div class="modal-body">' + content +
			'</div>' +
			'<div class="modal-footer">' +
			'<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>' +
			'<button type="button" class="btn btn-primary" id="popSubmitButton">确认</button>' +
			'</div>' +
			'</div></div></div>');
		$("body").append(alertHTML);
		alertHTML.modal('show');

		alertHTML.on('hidden.bs.modal', function (e) {
			alertHTML.remove();
		});

		$("#popSubmitButton").off("click").on("click",function(){
				okCallBack();
				alertHTML.modal('hide');
		});

	};

	POP.confirm_lg = function(title,content,okCallBack){

		var alertHTML = $('<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">' +
				'<div class="modal-dialog">' +
				'<div class="modal-content">' +
				'<div class="modal-header">' +
				'<h4 class="modal-title">'+title+'</h4>' +
				'</div>' +
				'<div class="modal-body">' + content +
				'</div>' +
				'<div class="modal-footer">' +
				'<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>' +
				'<button type="button" class="btn btn-primary" id="popSubmitButton">确认</button>' +
				'</div>' +
				'</div></div></div>');
		$("body").append(alertHTML);
		alertHTML.modal('show');

		alertHTML.on('hidden.bs.modal', function (e) {
			alertHTML.remove();
		});

		$("#popSubmitButton").off("click").on("click",function(){
			okCallBack();
			alertHTML.modal('hide');
		});

	};


	//警告提示框
	POP.warning = function(title,content){
		var $title   = '警告信息';
		var $content = '';

		if(arguments.length == 2){
			$title   = arguments[0] == 'undefined' ? '' : arguments[0];
			$content = arguments[1] == 'undefined' ? '' : arguments[1];
		}else{
			$content = arguments[0];
		}
		showNotify('notice',$title,$content);
	};

	//普通信息提示框
	POP.info = function(title,content){
		var $title   = '提示信息';
		var $content = '';

		if(arguments.length == 2){
			$title   = arguments[0] == 'undefined' ? '' : arguments[0];
			$content = arguments[1] == 'undefined' ? '' : arguments[1];
		}else{
			$content = arguments[0];
		}
		showNotify('info',$title ,$content);
	};

	//错误信息提示框
	POP.error = function(title,content){
		var $title   = '操作失败';
		var $content = '';

		if(arguments.length == 2){
			$title   = arguments[0] == 'undefined' ? '' : arguments[0];
			$content = arguments[1] == 'undefined' ? '' : arguments[1];
		}else{
			$content = arguments[0];
		}
		showNotify('error',$title ,$content);
	};


	//成功信息提示框
	POP.success = function(title,content){
		var $title   = '操作成功';
		var $content = '';

		if(arguments.length == 2){
			$title   = arguments[0] == 'undefined' ? '' : arguments[0];
			$content = arguments[1] == 'undefined' ? '' : arguments[1];
		}else{
			$content = arguments[0];
		}
		showNotify('success',$title ,$content);
	};


	//消息创建方法
	var showNotify = function(type,title,content){

		//根据类型使用不同的图标
		var iconClass = "";
		if(type == 'info'){
			iconClass = "glyphicons glyphicons-info-sign";
		}else if(type == 'error'){
			iconClass = "glyphicons glyphicons-remove-sign";
		}else if(type == 'success'){
			iconClass = "glyphicons glyphicons-ok-sign";
		}else if(type == 'notice') {
			iconClass = "glyphicons glyphicons-exclamation-sign";
		}

		PNotify.removeAll();

		new PNotify({
			icon : iconClass,
			animation : 'slide',
			title: title,
			text: content,
			type :type,
			delay : 5000,
			buttons: {
				closer : true
			}
		});
	};

	POP.startLoading = function(){

		// var nowScrollTop = $('html,body').scrollTop();

		// alert(nowScrollTop);

        // if(nowScrollTop > 0){
        //      $('html,body').animate({scrollTop:0},0);
		// }

        // $('html').css('overflow-y','hidden');

		$('body').loading({
			loadingWidth:180,
			title:'',
			name:'startloading',
			discription:'处理中...',
			direction:'column',
			type:'origin',
			// originBg:'#71EA71',
			originDivWidth:40,
			originDivHeight:40,
			originWidth:6,
			originHeight:6,
			smallLoading:false,
			loadingMaskBg:'rgba(0,0,0,0.2)'
		});

		// $('.cpt-loading-mask').css(top,"300px");
	}

	POP.endLoading = function(){
		removeLoading('startloading');
	}

	window.POP = POP;

	var LOADING = {};

	LOADING.createNoDataView = function(elem){

		var noDataIcon = [
			{icon : "glyphicons glyphicons-bath", "text" : "咦，没有搜索到相关数据，要不先泡个澡..."},
			{icon : "glyphicons glyphicons-cat", "text" : "咦，没有搜索到相关数据，要不先撸个猫..."},
			{icon : "glyphicons glyphicons-baby-formula", "text" : "咦，没有搜索到相关数据，要不先喝口奶..."},
			{icon : "glyphicons glyphicons-shopping-cart", "text" : "咦，没有搜索到相关数据，要不先购会物..."},
			{icon : "glyphicons glyphicons-bomb", "text" : "咦，没有搜索到相关数据，要不先点个雷..."}
		];

		var index = Math.floor((Math.random()*noDataIcon.length)); 

		var iconData = noDataIcon[index];

		var noDataView = '<div class="content_no_data_box"><span class="'+iconData.icon+'"></span>'+iconData.text+'</div>';

		$(".content_no_data_box").remove();
		elem.prepend(noDataView);
	
		
	};

	LOADING.removeNoDataView = function(){
		$(".content_no_data_box").remove();
	}
	
	window.LOADING = LOADING;

	/*
	 * 本地存储工具
	 */
  var customStorage = {};

	//session会话存储-保存
	customStorage.setSessionValue = function(key,value){

		//判断是否支持端session存储
		if(!window.sessionStorage){
			POP.error("抱歉您的浏览器不支持端session存储功能!");
			return;
		}

		//判断key与value是否有值
		if(key == undefined || value == undefined){
			POP.error("请您传递需要缓存的session键名与键值!");
			return;
		}

		//存储内容
		window.sessionStorage.setItem(key,value);


	};

	//session会话存储-获取
	customStorage.getSessionValue = function(key){

		//判断是否支持端session存储
		if(!window.sessionStorage){
			POP.error("抱歉您的浏览器不支持端session存储功能!");
			return;
		}

		//判断key是否有值
		if(key == undefined){
			POP.error("请您传递需要缓存的session键名与键值!");
			return;
		}

		//存储内容
		return window.sessionStorage.getItem(key);


	};

	//session会话存储-删除
	customStorage.deleteSessionValue = function(key){
		window.sessionStorage.removeItem(key);
	};

	//session会话存储-删除全部
	customStorage.deleteSessionValueAll = function(){
		window.sessionStorage.clear();
	};

	//本地持久化存储-保存
	customStorage.setPersistenceValue = function(key,value){

		//判断是否支持端本地存储
		if(!window.localStorage){
			POP.error("抱歉您的浏览器不支持端session存储功能!");
			return;
		}

		//判断key与value是否有值
		if(key == undefined || value == undefined){
			POP.error("请您传递需要缓存的session键名与键值!");
			return;
		}

		//存储内容
		window.localStorage.setItem(key,value);

	};

	//本地持久化存储-获取
	customStorage.getPersistenceValue = function(key){

		//判断是否支持端本地存储
		if(!window.localStorage){
			POP.error("抱歉您的浏览器不支持端session存储功能!");
			return;
		}

		//判断key是否有值
		if(key == undefined){
			POP.error("请您传递需要缓存的session键名与键值!");
			return;
		}

		//存储内容
		return window.localStorage.getItem(key);

	};

	//本地持久化存储-删除
	customStorage.deleteLocalStorageValue = function(key){
		window.localStorage.removeItem(key);
	};

	//本地持久化存储-删除全部
	customStorage.deleteLocalStorageValueAll = function(){
		window.localStorage.clear();
	};

	window.customStorage = customStorage;
