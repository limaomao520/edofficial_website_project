//获取URL传参
function GetRequest() {
	var url = location.search; //获取url中"?"符后的字串
	var theRequest = new Object();
	if(url.indexOf("?") != -1) {
		var str = url.substr(1);
		strs = str.split("&");
		for(var i = 0; i < strs.length; i++) {
			theRequest[strs[i].split("=")[0]] = unescape(strs[i].split("=")[1]);
		}
	}


	return theRequest;
}

//去除两边空格
function str_trim(str){
	return str.replace(/(^\s*)|(\s*$)/g, "");
}

function insert_flg(str,flg,sn){
    var newstr="";
    for(var i=0;i<str.length;i+=sn){
        var tmp=str.substring(i, i+sn);
        newstr+=tmp+flg;
    }
    return newstr;
}

function GetLength(str) {
  ///<summary>获得字符串实际长度，中文2，英文1</summary>
  ///<param name="str">要获得长度的字符串</param>
  var realLength = 0, len = str.length, charCode = -1;
  for (var i = 0; i < len; i++) {
    charCode = str.charCodeAt(i);
    if (charCode >= 0 && charCode <= 128) 
       realLength += 1;
    else
       realLength += 2;
  }
  return realLength;
};

function getLocalTime(nS) {     
	return new Date(parseInt(nS) * 1000).toLocaleString().replace(/:\d{1,2}$/,' ');     
 } 

function dateFormat(format,unix) {

	var newDate = new Date(unix*1000);

	var date = {
		   "M+": newDate.getMonth() + 1,
		   "d+": newDate.getDate(),
		   "h+": newDate.getHours(),
		   "m+": newDate.getMinutes(),
		   "s+": newDate.getSeconds(),
		   "q+": Math.floor((newDate.getMonth() + 3) / 3),
		   "S+": newDate.getMilliseconds()
	};
	if (/(y+)/i.test(format)) {
		   format = format.replace(RegExp.$1, (newDate.getFullYear() + '').substr(4 - RegExp.$1.length));
	}
	for (var k in date) {
		   if (new RegExp("(" + k + ")").test(format)) {
				  format = format.replace(RegExp.$1, RegExp.$1.length == 1
						 ? date[k] : ("00" + date[k]).substr(("" + date[k]).length));
		   }
	}
	return format;
}

function checkPhone(phoneValue){
	var regExp = /^(86)?((13\d{9})|(15[0,1,2,3,5,6,7,8,9]\d{8})|(18[0,5,6,7,8,9]\d{8}))$/;
	if(!regExp.test(phoneValue)){
		return false;
	}
	return true;
}

function phone_ti(str){
    var newstr = str.split('');
    newstr.splice(3,1,"*");
    newstr.splice(4,1,"*");
    newstr.splice(5,1,"*");
    newstr.splice(6,1,"*");
    return newstr.join("");
}

//参数解密
function deciphering(str){
    var base = new Base64();  
    return base.decode(str + "");  
};

function containsArr(arr, obj) {
	var i = arr.length;
	while(i--) {
		if(arr[i] === obj) {
			return true;
		}
	}
	return false;
}

//随机字符串
function randomString(len,type) {
	len = len || 32;
	if (type == 'number') {
		var $chars = '123456789';
	} else{
		var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
	}
	/****默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1****/
	var maxPos = $chars.length;
	var pwd = '';
	for(var i = 0; i < len; i++) {
		pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
	}
	return pwd;
}