### 生成图片水印
<hr/>
#### 本节讲解如何为图片打水印，图片水印设置接口为 /systemApi/setImageWatermark，该接口主要需要注意3个 POST 参数的使用，下面分别进行一下说名：
<br/>
参与1：srcImageUrl（必传），该参数需要传递一个完整的图片路径地址，作为原图
<br/><br/>
参数2：waterMarkUrl（非必传），该参数传递需要打印的水印图片（完整路径），如果不传该参数，需要在图片相关的全局配置中配置默认水印图片参数，在参数小节中有说明
<br/><br/>
参数3：position（非必传），表示水印在原图中的位置，不传默认在右下角，还支持以下几个设置：
<br/><br/>
leftTop 左上
<br/><br/>
leftBottom 左下
<br/><br/>
rightTop 右上
<br/><br/>
rightBottom 右下
<br/><br/>
center 中间
<br/><br/>
#### 下面我们实际上传一个例子，首先准备一张原图（完整路径），水印图片我们使用系统默认的水印图片，访问接口：http://127.0.0.1:8008/systemApi/setImageWatermark，该请求为POST请求，参数传递如下：
<br/>
```javascript
{
    "srcImageUrl":"http://127.0.0.1:8008/images/ceshitouxiang.png"
}
```
<br/>
#### 上传后，会将我们的原图使用系统水印图片进行合并，结果如下：
<br/>
<img src="/image/tutorial-27.png" width="40%" style="border:1px solid #ccc;" />

