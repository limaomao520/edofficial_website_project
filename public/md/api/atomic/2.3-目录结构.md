### 目录结构
<hr/>
<img src="/image/tutorial-4.png" width="40%" style="border:1px solid #ccc;" />
<br/><br/>
#### 目录相关作用简介
<br/>
#### <font color="#666">- api目录：主要用于放置，用户的相关接口声明文件，用户将所有原子层接口都可以再此进行声明配置</font>
<br/>
#### <font color="#666">- business目录：主要用于放置接口的实现文件，除了自动构建的接口外，所有需要用户具体实现的接口都放置在此处</font>
<br/>
#### <font color="#666">- cache目录：主要用于放置相关文件缓存的目录，如果缓存类型为文件缓存，将会在此处生成相关的缓存文件</font>
<br/>
#### <font color="#666">- composerLib目录：可以将composer安装的第三方库放置在此处，系统会自动将其引入，在接口实现文件中就可以进行使用</font>
<br/>
#### <font color="#666">- config目录：放置用户项目的所有配置文件，例如全局变量配置，框架相关配置，白名单配置，正则配置等</font>
<br/>
#### <font color="#666">- extension目录：放置扩展模块的接口声明文件与相关实现目录，在安装扩展模块后会将相关文件放置在本目录，用户一般无需手动修改</font>
<br/>
#### <font color="#666">- framework目录：框架核心程序目录，接口路由，各种功能类引入，缓存处理等</font>
<br/>
#### <font color="#666">- script目录：存放一些php脚本文件</font>
<br/>
#### <font color="#666">- temp目录：放置一些临时目录文件，例如扩展模块压缩包下载后，在此处进行解压分发</font>
<br/>
#### <font color="#666">- .htaccess文件：路由重写相关文件（apache使用），省略请求所需的入口文件路径所用</font>
<br/>
#### <font color="#666">- index.php文件：项目入口文件，所有接口请求的入口</font>
<br/>
#### 各个常用目录下内容介绍
<br/>
#### api目录：
<br/>
<img src="/image/tutorial-11.png" width="30%" style="border:1px solid #ccc;" />
<br/><br/>
#### <font color="#666">- api.php文件，该文件用于声明接口的相关信息</font>
<br/>
#### config目录：
<br/>
<img src="/image/tutorial-12.png" width="30%" style="border:1px solid #ccc;" />
<br/><br/>
#### <font color="#666">- appConfig.php文件，进行项目的相关配置，例如数据库配置，是否验权，是否开启调试信息等</font>
<br/>
#### <font color="#666">- global.php文件，进行全局常量的配置，配置后，可在所有接口文件中使用</font>
<br/>
#### <font color="#666">- interfaceWhitelists.php文件，接口与服务器白名单配置</font>
<br/>
#### <font color="#666">- regexpConfig.php文件，接口参数验证正则配置</font>
<br/>
#### extension目录：
<br/>
<img src="/image/tutorial-13.png" width="30%" style="border:1px solid #ccc;" />
<br/><br/>
#### <font color="#666">- apiConfig目录，主要用于方式扩展模块的接口声明信息</font>
<br/>
#### <font color="#666">- apiLib.php文件，用于根据规则自动引入扩展模块的接口声明信息</font>
<br/>
