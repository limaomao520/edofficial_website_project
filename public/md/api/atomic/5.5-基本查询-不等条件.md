### 基本查询 - 不等条件
<hr/>
#### 接着上节的内容，在日常需求中，我们会碰到一个查询场景就是，查询所有不等于某些条件的情况，我们继续以 student 表为例，查询学生ID不等于2的所有学生，观察一下下面的接口：
<br/>
#### http://127.0.0.1/atomic-layer-php-v1.0/student/search<font color="red">/no/s_id-2</font>
<br/>
#### 请求结果
<br/>
```javascript
{
  "success": true,
  "result": {
    "count": 0,
    "data": [
      {
        "s_id": "3",
        "s_name": "王五",
        "s_age": "3",
        "s_delete": "0"
      },
      {
        "s_id": "8",
        "s_name": "user1",
        "s_age": "100",
        "s_delete": "0"
      }
    ]
  },
  "interfaceInfo": [
    "请求方式：GET",
    "请求源地址：http://127.0.0.1",
    "请求接口：/student/search",
    "SQL语句: select * from mm_student where  (s_id!=2) limit 0,20",
    "是否使用缓存结果：否"
  ]
}
```
<br/>
#### 观察SQL语句，已经为我们构建出了不等条件的查询SQL，下小节中，我们讲解如何使用 范围条件查询