### 基本查询 - 包含条件
<hr/>
#### 接着上节的内容，在日常需求中，我们会碰到一个查询场景就是，包含某些值的条件，例如查询所有年龄中包含1岁，2岁和3岁的学生，观察下边的接口：
<br/>
#### http://127.0.0.1/atomic-layer-php-v1.0/student/search<font color="red">/in/`s_age`-1,2,3</font>
<br/>
#### 请求结果
<br/>
```javascript
{
  "success": true,
  "result": {
    "count": 0,
    "data": [
      {
        "s_id": "2",
        "s_name": "李四",
        "s_age": "2",
        "s_delete": "0"
      },
      {
        "s_id": "3",
        "s_name": "王五",
        "s_age": "3",
        "s_delete": "0"
      }
    ]
  },
  "interfaceInfo": [
    "请求方式：GET",
    "请求源地址：http://127.0.0.1",
    "请求接口：/student/search",
    "SQL语句: select * from mm_student where s_age in('1','2','3') limit 0,20",
    "是否使用缓存结果：否"
  ]
}
```
<br/>
#### 观察构建出来的SQL，为我们构建了IN条件查询语句，下一节中我们讲解模糊查询
