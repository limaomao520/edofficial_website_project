### 基本查询 - 或者条件
<hr/>
#### 接着上节的内容，我们如果需要查询另一个种情况，比如查询学生年龄为2，或者学生姓名为李四的，这样的查询需求，该如何实现，观察下边的接口地址：
<br/>
#### http://127.0.0.1/atomic-layer-php-v1.0/student/search<font color="red">/or/s_name-张三,李四</font>
<br/>
#### 查询结果
<br/>
```javascript
{
  "success": true,
  "result": {
    "count": 0,
    "data": [
      {
        "s_id": "2",
        "s_name": "李四",
        "s_age": "2",
        "s_delete": "0"
      }
    ]
  },
  "interfaceInfo": [
    "请求方式：GET",
    "请求源地址：http://127.0.0.1",
    "请求接口：/student/search",
    "SQL语句: select * from mm_student where  (s_name='张三' or s_name='李四') limit 0,20",
    "是否使用缓存结果：否"
  ]
}
```
<br/>
#### 我们观察一下SQL语句就明白了，系统会自动为我们构建出这样的SQL，这里观察后我们发现，使用的是同一个字段作为条件的，如果要使用不同的字段作为条件呢，例如查询 学生名称为张三 或者 年龄为2，观察下边的请求接口：
<br/>
#### http://127.0.0.1/atomic-layer-php-v1.0/student/search<font color="red">/or/`s_name`-张三|`s_age-2`</font>
<br/>
#### 请求结果
<br/>
```javascript
{
  "success": true,
  "result": {
    "count": 0,
    "data": [
      {
        "s_id": "2",
        "s_name": "李四",
        "s_age": "2",
        "s_delete": "0"
      }
    ]
  },
  "interfaceInfo": [
    "请求方式：GET",
    "请求源地址：http://127.0.0.1",
    "请求接口：/student/search",
    "SQL语句: select * from mm_student where ((s_name='张三') or (s_age='2')) limit 0,20",
    "是否使用缓存结果：否"
  ]
}
```
<br/>
#### 如果我们需要将 或者查询 和 相等查询同时使用，例如查询 学生年龄为2 或者 学生姓名为张三，并且学生ID为2的条件，观察下边的接口：
<br/>
#### http://127.0.0.1/atomic-layer-php-v1.0/student/search<font color="red">/or/`s_name`-张三|`s_age`-2/s_id/2</font>
<br/>
#### 我们观察一下构建出来的SQL：
<br/>
```javascript
{
  "success": true,
  "result": {
    "count": 0,
    "data": [
      {
        "s_id": "2",
        "s_name": "李四",
        "s_age": "2",
        "s_delete": "0"
      }
    ]
  },
  "interfaceInfo": [
    "请求方式：GET",
    "请求源地址：http://127.0.0.1",
    "请求接口：/student/search",
    "SQL语句: select * from mm_student where s_id = '2' and ((s_name='张三') or (s_age='2')) limit 0,20",
    "是否使用缓存结果：否"
  ]
}
```
<br/>
#### 是否很强大呢，下节中我们讲解一下另一个常用的查询，不等条件查询