### 项目配置
<hr/>
#### <p style="color:#666;font-size:14px;line-height:26px;">项目的基本配置文件，在config/appConfig.php，里面给出了所有项目所支持的配置项，可以进行相关的修改配置，下面详细介绍一下所支持的配置项，如下所示：</p>
<br/>
```PHP
/*
 * 应用基本配置数组
 */
$GLOBALS['settings'] = array(
    
    //设置调试模式
    //开启后可输出一些比如请求方式，数据请求时的SQL语句等基本信息，用于简单的错误调试
    "debug"	=> true,
    
    //接口相关日志信息存放数组（请勿变更）
	"interfaceInfo" => array(),

    //是否为独立部署(无聚合层)
    //如开启后，表示原子层可以独立进行部署，会跳过相关聚合层与原子层之间的验权操作
	"isIndependentDeployment" => false,

    //是否需要聚合层验权
    //如开启后，聚合层访问原子层时，需要进行验权操作，否则无法访问原子层服务
	"isAggregationServerValidation" => false,

    //是否开启物理删除
    //开启后，在接口实现用，可以调用数据库提供的Delete删除方式，否则表示只可使用逻辑删除（更新）来进行删除
	"isPhysicallyDelete" => true,

	//是否开启跨域请求
	"isAccessControlAllowOrigin" => false,

    //设置允许的请求模式
    //默认为支持三种请求方式，如需要进行其他类型的请求，可以增加
	"method" => array('GET','POST','ALL'),

    //是否允许自动构建标准CURD
    //开启后，在接口声明中如该接口要使用自动构建，则就可以进行相关的构建工作
	"isCURD" => true,

    //默认查询时分页显示数量
    //查询操作时，如不设置分页数，默认返回的条数
	"pageLimit" => 20,
	
    //是否开启接口白名单验证
    //如开启，则聚合层访问时，需要进行白名单的验证，该操作和验权操作是分别进行验证的
	"isWhiteAuth" => false,

    //缓存相关配置
    //接口配置时的缓存处理相关信息
	"cache" => array(

        //是否进行缓存
        //开启后，就可以在接口声明中也进行缓存声明
		"isCache" => false,

        //缓存的类型 file:文件缓存
        //当前只支持文件类型的数据缓存，暂不支持内存型缓存
		"cacheType" => 'file',

        //缓存的目录 根目录/目录名称
		"cacheDir" => 'cache',
		
        //缓存的过期时间(秒)
        //文件缓存的过期时间，过期后，会重新生成该文件缓存
		"cacheValidity" => 3600

    ),
    
    //是否可以不接入数据库直接使用
    //如果开启，则表示项目安装完毕后，必须首先要配置数据库相关信息，并保证连接成功，才可进行使用
	"noDatabaseUse" => false,

	//数据库相关配置，将所有出现在配置项中的数据库进行连接以及操作对象的创建
	"database" => array(

		//Mysql相关配置项
		"Mysql" => array(
			"username"    => 'root',
			"password"    => '',
			"port"        => 3306,
			"host"        => '',
			"dbname"   	  => ''
		)
    )
    
);
```