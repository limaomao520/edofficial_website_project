### 入口文件
<hr/>
#### <p style="color:#666;font-size:14px;line-height:26px;">Easy-D原子层开发框架，的入口文件为项目根目录中的 index.php 文件，该文件内容如下：</p>
<br/>
```PHP
//项目根目录
define('BASE_PATH',str_replace('\\','/',realpath(dirname(__FILE__).'/')));

//项目核心框架目录
define('FRAMEWORK_PATH',BASE_PATH."/framework");

//项目业务目录
define('BUSINESS_PATH',BASE_PATH."/business");

//项目扩展业务目录
define('EXTENSION_PATH',BASE_PATH."/extension");

//导入核心类库
require_once(FRAMEWORK_PATH."/core/core.php");
```
<br/>
#### 该入口文件定义了一些全局所要用到的目录常量，以及引入框架的核心文件
<br/>
#### 建议通过路由重写机制，隐藏掉访问接口时的入口文件名称，框架下载后，会携带一个APACHE的路由重写处理文件 .htaccess
<br/>
#### 完整的访问项目地址路径应当为：http://服务器IP/项目名称/入口文件/接口地址
<br/>
#### 例如：http://127.0.0.1/demoProject/index.php/demo/test
<br/>
将路由重写后的访问地址为：
<br/>
<br/>
#### 地址：http://127.0.0.1/demoProject/demo/test（推荐使用，更加便捷，地址结构更加清晰）
<br/>