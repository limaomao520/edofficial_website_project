### 基本接口创建
<br/>
#### 本节我们来讲解一下如何在结合层来创建一个接口，我们首先找到聚合层项目中的 <font color="red">/middleBusiness/api</font> 目录，在api目录下有两个文件，分别为 api.js 和 core_api.js
<br/>
#### <font color="#666">api.js 主要作为接口的声明文件</font>
<br/>
#### <font color="#666">core_api.js 主要作为原子层接口地址的存放文件，在聚合层中如果需要对原子层接口进行访问，我们需要将用到的原子层接口存放在这个文件内，方便聚合层接口的实现文件里进行调用</font>
<br/>
#### 下面我们在 api.js 文件中声明一个基本接口，使用方式和原子层类似，如下所示：
<br/>
```javascript
"/easyD/demo" : {
    type   : 'BUSSINESS',
    method : 'GET'
}
```
<br/>
#### 我们来对该接口创建实现文件，进行相关的具体实现，在 <font color="red">/middleBusiness/routes</font> 目录下创建 easyD 目录，在easyD目录下创建 demo.js 文件，实现文件内容如下所示：
<br/>
```javascript
exports.demo = function(req,res){
    RES.successResponse(res,"hello easyD!");
}
```
<br/>
#### 我们进行一下该接口的访问，访问地址为：http://127.0.0.1:8001/easyD/demo (我们该聚合层启动时的端口号为8001)，返回结果如下：
<br/>
```javascript
{
  "success": true,
  "result": "hello easyD!"
}
```
<br/>
#### 我们看到已经可以正确的进行访问了 <font color="red">（注：在每次修改接口实现文件后，我们需要重新启动一下聚合层，我们也可以通过守护进程或者监听的方式进行启动聚合层，这样就不用在每次修改完文件后重新启动，在本次教程中，我们没有使用自动启动，都是每次修改后进行手动启动的，监听和守护进行的启动方式我们会在后面的章节专门进行介绍）</font>
<br/>
#### 如果我们的接口需要进行一些参数的验证操作，那么我们可以在接口声明时，进行如下选项的添加：
<br/>
```javascript
"/easyD/demo" : {
    type   : 'BUSSINESS',
    method : 'GET',
    getMustParams : {
        p1 : '',
        p2 : 'NUMBER'
     }
}
```
<br/>
#### 上面意思为我们在访问该接口时需要传递两个GET必传参数p1和p2，p1不能为空,p2必须为数字，我们在没有写入该参数的情况下再次访问会出现如下结果 <font color="red">（不要忘记修改了内容，要重新启动服务）</font>：
<br/>
```javascript
{
  "success": false,
  "result": {
    "code": "bussiness_error",
    "message": "缺少RESTFUL参数 :p1,p2"
  }
}
```
<br/>
#### 我们这次增加上这两个接口的get参数，地址为：http://127.0.0.1:8001/easyD/demo/p1/呵呵/p2/100（使用restful的参数传递方式，不是传统的get方式），再次进行访问，会发现已经可以正常访问了
<br/>
#### 至此接口的基本创建我们就讲解到这里