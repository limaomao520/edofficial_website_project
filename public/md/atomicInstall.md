### <font color="red">一. 原子层独立部署安装（Php框架）</font>
<br/>
#### 1.1 下载原子层框架，点击[【下载最新版本】](http://www.baidu.com)进行下载 
<br/>
#### 项目目录结构预览如下
<br/>
<img src="/image/tutorial-4.png" width="40%" style="border:1px solid #ccc;" />
<br/><br/>
#### 目录相关作用简介
<br/>
#### <font color="#666">- api目录：主要用于放置，用户的相关接口声明文件，用户将所有原子层接口都可以再此进行声明配置</font>
<br/>
#### <font color="#666">- business目录：主要用于放置接口的实现文件，除了自动构建的接口外，所有需要用户具体实现的接口都放置在此处</font>
<br/>
#### <font color="#666">- cache目录：主要用于放置相关文件缓存的目录，如果缓存类型为文件缓存，将会在此处生成相关的缓存文件</font>
<br/>
#### <font color="#666">- composerLib目录：可以将composer安装的第三方库放置在此处，系统会自动将其引入，在接口实现文件中就可以进行使用</font>
<br/>
#### <font color="#666">- config目录：放置用户项目的所有配置文件，例如全局变量配置，框架相关配置，白名单配置，正则配置等</font>
<br/>
#### <font color="#666">- extension目录：放置扩展模块的接口声明文件与相关实现目录，在安装扩展模块后会将相关文件放置在本目录，用户一般无需手动修改</font>
<br/>
#### <font color="#666">- framework目录：框架核心程序目录，接口路由，各种功能类引入，缓存处理等</font>
<br/>
#### <font color="#666">- script目录：存放一些php脚本文件</font>
<br/>
#### <font color="#666">- temp目录：放置一些临时目录文件，例如扩展模块压缩包下载后，在此处进行解压分发</font>
<br/>
#### <font color="#666">- .htaccess文件：路由重写相关文件（apache使用），省略请求所需的入口文件路径所用</font>
<br/>
#### <font color="#666">- index.php文件：项目入口文件，所有接口请求的入口</font>
<br/>
#### 1.2 将该框架放入网站根目录，并自定义一个项目名称（例如：atomic-layer-php-v1.0），例如: `/var/www/html/atomic-layer-php-v1.0`
<br/>
#### 1.3 修改项目配置文件，进行一些初始化的相关配置信息，打开config/appConfig.php，独立部署使用原子层，先确保以下两项配置如下：
<br/>
```PHP
//是否为独立部署(无聚合层)
"isIndependentDeployment" => true

//是否需要聚合层验权
"isAggregationServerValidation" => false

//是否可以不接入数据库直接使用
"noDatabaseUse" => true
```
<br/>
#### 1.4 配置完成后，保证Lamp环境已启动，打开浏览器或接口测试工具，访问：http://<font color="red">服务器IP地址 或 域名</font>/atomic-layer-php-v1.0，如显示如下内容，则表示原子层框架已安装完成，例如：http://127.0.0.1/atomic-layer-php-v1.0 <font color="red">（注意：此处没有写入口文件，index.php，因为使用了apache路由重写）</font>
<br/>
#### <font color="#666">- 实际完整的访问路径为：http://127.0.0.1/atomic-layer-php-v1.0/index.php</font>
<br/>
```javascript
{
  "success": false,
  "result": {
    "code": "00002",
    "message": "接口地址为空"
  }
}
```
<br/>
### <font color="red">二. 创建第一个自己的接口</font>
<br/>
#### 2.1 声明一个接口，打开api目录中的api.php文件，在内部声明一个接口文件，如下：<font color="red">（注意别忘了<?php ?>包含标签）</font>
<br/>
```PHP
/*
 * 接口配置文件
 */
return array(
	
	//测试接口
	'/demo/test' => array(
	   'method' => 'GET'
	)

);
```
<br/>
#### 2.2 创建该接口的实现文件，打开目录business创建demo目录（也就是接口中的/<font color="red">demo</font>/test，此处代表目录），在demo目录中创建test.php文件（也就是接口文件中的/demo/<font color="red">test</font>，此处代表文件），在test.php文件中写入如下内容：
<br/>
```PHP
echo "hello world!!!";
```
<br/>
#### 2.3 接口创建完毕（就是这么简单），打开浏览器或者接口测试工具，访问：http://<font color="red">服务器IP地址 或 域名</font>/atomic-layer-php-v1.0/demo/test
<br/>
#### <font color="#666">- 例如：http://127.0.0.1/atomic-layer-php-v1.0/demo/test</font>，如成功，将如期打印出hello world!!!
<br/>
#### 建议：
<p style="line-height:22px;margin-top:5px;"><font color="#666">一般来说，我们不推荐您单独使用，原子层框架进行项目的开发，引入原子层框架对数据结构的处理比较单一，并且其中的大量的内置接口，主要服务于聚合层业务和管理端业务，配合聚合层+管理端会使您的项目开发更加事半功倍，并且会拥有一个良好的初期项目架构，单一的原子层开发比较适合于需要进行一些基本接口业务开发（也可进行复杂接口的开发，因为接口实现文件中具有比较灵活的开发方式，您可以根据需求编写任何实现业务），或者对整体结构没有什么过多要求的项目。</font></p>
<br/>
#### <p style="line-height:40px;"><font color="red">到此，原子层接口框架的安装以及接口声明实现完成，更多原子层服务框架的使用方式可以查看Api文档教程</font></p>

