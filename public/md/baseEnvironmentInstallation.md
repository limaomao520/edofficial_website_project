### <font color="red">一. 环境介绍</font>
<br/>
Easy-D集合开发框架，完整安装下，主要依赖于两个后台环境，Php开发环境以及,nodejs开发环境，使用Php后台作为 [**原子层**](#atomic) 
的开发环境，充分考虑到了Php使用的广泛性以及便捷性，拥有大量三方库以及对各第三方应用SDK都有较好的支持， [**聚合层**](#aggregation) 使用nodejs环境，是为了更好的让前端开发者快速上手进行相关业务接口的开发工作，真正实现前端全栈界面+业务接口开发。
<br/><br/>
### <font color="red">二. Linux（CentOS 7）环境安装</font>
<br/>
#### 安装Php (Lamp) 开发环境,快速安装（Yum方式）
<br/>
#### 2.1 更新yum源
<br/>
```Nginx
yum update
```
<br/>
#### 2.2 安装apache
<br/>
```Nginx
yum install httpd httpd-devel
```
<br/>
#### 2.3 安装完成后启动apache
<br/>
```Nginx
service httpd start
```
<br/>
#### 2.4 访问服务器地址进行测试
<br/>
在浏览中访问 **http://主机IP地址 或 域名**，如看到以下结果，则证明apache服务器安装成功
<br/><br/>
<img src="/image/tutorial-1.jpeg" width="100%" style="border:1px solid #ccc;" />
<br/><br/>
#### 2.5 安装Php引擎
<br/>
```Nginx
#更新yum源
rpm -Uvh https://mirror.webtatic.com/yum/el7/epel-release.rpm
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

#查看yum源是否有php7.1安装包
yum list php71

#安装7.1以及相关扩展
yum install -y php71w-fpm php71w-opcache php71w-cli php71w-gd php71w-imap php71w-mysqlnd php71w-mbstring php71w-mcrypt php71w-pdo php71w-pecl-apcu php71w-pecl-mongodb php71w-pecl-redis php71w-pgsql php71w-xml php71w-xmlrpc php71w-devel mod_php71w
```
<br/>
#### 2.6 重新启动apache服务器
<br/>
```Nginx
service httpd restart
```
<br/>
#### 2.7 编写一个php测试脚本文件，在/var/www/html (网站默认根目录) 目录下创建一个index.php，内容如下
<br/>
```Java
<?php

  phpinfo();

?>
```
<br/>
#### 2.8 访问该文件，如输出以下画面则表示安装成功
<br/>
<img src="/image/tutorial-2.png" width="50%" style="border:1px solid #ccc;" />
<br/><br/>
#### 2.9 安装mysql数据库，依次执行以下命令
<br/>
```Nginx
wget http://dev.mysql.com/get/mysql-community-release-el7-5.noarch.rpm

rpm -ivh mysql-community-release-el7-5.noarch.rpm

yum install mysql-community-server
```
<br/>
#### 2.10 启动mysql,此时mysql数据没有连接密码，密码为空
<br/>
```Nginx
service mysqld start
```
<br/>
#### 2.11 在当前服务器中通过一下命令，连接mysql测试，是否可以连接成功，如出现以下画面，则表示mysql安装完成，版本为：5.6.42
<br/>
```SQL
mysql -uroot

Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 2
Server version: 5.6.42 MySQL Community Server (GPL)

Copyright (c) 2000, 2018, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> 
```
<br/>
#### 2.12 为mysql设置一个管理用户，并设置具有远程连接的权限(非必须)，使用MYSQL工具workBench进行连接测试，如下则表示远程连接设置成功
<br/>
```SQL
grant usage on *.* to 'limaomao'@'localhost' identified by '123456'; --创建用户limaomao密码123456

grant all privileges on *.* to limaomao@'%'identified by '123456';   --设置可以远程访问,以及远程访问密码为123456
```
<br/>
<img src="/image/tutorial-3.png" width="50%" style="border:1px solid #ccc;" />
<br/><br/>
### <font color="red">三. Linux（CentOS 7）安装Nodejs环境</font>
<br/>
#### 3.1 安装编译所需工具gcc，make，openssl，wget（如安装过，不需要进行安装，也可覆盖安装）
<br/>
```Nginx
yum install -y gcc make gcc-c++ openssl-devel wget
```
<br/>
#### 3.2 下载源代码包
<br/>
```Nginx
wget https://nodejs.org/dist/v9.3.0/node-v9.3.0.tar.gz
```
<br/>
#### 3.3 解压下载后的源代码包（进入到下载的相关目录）
<br/>
```Nginx
tar -xf node-v9.3.0.tar.gz
```
<br/>
#### 3.4 进入解压后的目录，编译安装（时间较长，请耐心等待）
<br/>
```Nginx
cd node-v9.3.0       #进入源代码所在路径 

./configure          #先执行配置脚本 

make && make install #编译与部署 
```
<br/>
#### 3.5 完成后，执行以下命令进行相关测试，如结果如下，则表示nodejs已成功安装完成
<br/>
```Nginx
node -v  #执行结果：v9.3.0（版可能有所不同，不影响）

npm -v   #执行结果：5.5.1（版可能有所不同，不影响）
```
<br/>
### <font color="red">至此基本环境全部安装完成，可以进入框架的安装部分，Winodws版本的安装在视频中会讲到</font>
<br/>
<hr/>
<br/>
### 备注部分：
<br/>
#### <a name="atomic"></a> - 术语解释：原子层
<br/>
原子层，指基础的数据层，该层不包含具体的业务需求，是对一些基础数据表的CURD操作，主要用来作为业务聚合层的具体业务拼接使用。
<br/><br/>
#### <a name="aggregation"></a> - 术语解释：聚合层
<br/>
聚合层，主要使用原子层提供的数据接口进行具体业务的拼接，用于完成各种业务需求。