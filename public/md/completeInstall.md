### <font color="red">一. 原子层部署安装（Php框架），教程详见：独立部署:Hello world </font>
<br/>
#### 1.1 请先确保，完成原子层部署安装，并能够正确访问
<br/>
#### 1.2 配置连接MYSQL数据库，打开config/appConfig.php文件，找到如下数据库相关配置信息，填写相关信息<font color="red">（注：数据库需提前进行创建）</font>
<br/>
```PHP
"Mysql" => array(
	"username"    => '远程登录帐号',
	"password"    => '远程登录密码',
	"port"        => '端口号码',
	"host"        => '数据库连接地址',
	"dbname"   	  => '数据库名称'
)
```
<br/>
#### 1.3 打开数据库连接配置信息，设置为需要连接数据库进行使用
<br/>
```PHP
//是否可以不接入数据库直接使用
"noDatabaseUse" => false,
```
<br/>
#### 1.4 访问一个核心层的测试接口（可自己创建），如无任何报错信息，则表示数据库已连接成功，否则会报出相应的错误信息，例如：这里写一个错误的帐号，会显示以下返回，可根据错误在做具体调整
<br/>
```javascript
{
  "success": false,
  "result": "Mysql_Error:The user specified as a definer ('liweixuan2'@'36.47.160.84') does not exist"
}
```
<br/>
#### 1.5 调用原子层内置接口，进行管理端相关数据表和视图的安装工作，接口地址如下（可在浏览器或接口测试工具中调用），接口地址：http://原子层服务器地址/原子层项目名称/systemApi/installTableAndView
<br/>
#### 例如：http://127.0.0.1/atomic-layer-php-v1.0/systemApi/installTableAndView，如返回如下结果，则表示系统数据表创建完成
<br/>
```javascript
{
  "success": true,
  "result": "系统内置数据表创建成功"
}
```
<br/>
#### 1.6 上述步骤执行完成后，将为您创建18张系统内置表和4张视图，如下所示：
<br/>
<img src="/image/tutorial-7.png" width="30%" style="border:1px solid #ccc;" />
<br/><br/>
#### <font color="red">如果上述数据库建表接口出现错误，可以手动进行创建，可以找到核心层目录中的 /framework/install/install.sql 文件，手动拷贝内容在需要数据库中执行，完成表和视图的创建工作</font>
<br/><br/>
#### 至此：原子层的准备工作全部完成
<br/>
### <font color="red">二. 聚合层部署安装（Nodejs框架），教程详见：分层部署:Hello world </font>
<br/>
#### 2.1 请先确保，完成聚合层部署安装
<br/>
#### 2.2 启动聚合层服务
<br/>
### <font color="red">三. 管理端安装（Webpack+Vue）</font>
<br/>
#### 3.1 下载管理端框架，点击[【下载最新版本】](http://www.baidu.com)进行下载
<br/>
<img src="/image/tutorial-6.png" width="30%" style="border:1px solid #ccc;" />
<br/><br/>
#### 3.2 目录相关作用简介
<br/>
#### <font color="#666">- build目录：项目编译配置相关目录，webpack相关打包配置处理都在此处进行配置</font>
<br/>
#### <font color="#666">- client目录：项目的开发主目录，包含项目中所用到的路由，数据，页面等和项目有关的所有文件信息</font>
<br/>
#### <font color="#666">- dist目录：打包后的文件的存放目录，打包后的项目可直接放置在服务器中进行使用</font>
<br/>
#### <font color="#666">- node_modules目录：放置Nodejs的第三方模块目录，此目录为系统自带的安装目录，用户无需手动进行修改操作</font>
<br/>
#### <font color="#666">- static目录：存放一些不需要打包的静态文件，打包后会原样写入到打包目录中</font>
<br/>
#### <font color="#666">- template目录：模板目录，存放一些自动生成文件所需的模板文件，用户无需改动</font>
<br/>
#### <font color="#666">- tmp目录：临时目录，用于存放一些系统在使用过程中生成的临时文件，用户无需改动</font>
<br/>
#### <font color="#666">- .babelrc文件：用于设置一些ES5,6的兼容语法配置，用户无需改动</font>
<br/>
#### <font color="#666">- .gitignore文件：设置项目中在GIT管理状态下，忽略GIT管理的文件或目录</font>
<br/>
#### <font color="#666">- package.json文件：存放所有需要安装的三方包内容</font>
<br/>
#### <font color="#666">- postcss.config文件：设置自动生成浏览器兼容性前缀的相关配置</font>
<br/>
#### 3.3 将该框架放置在您要进行开发机器的任意地方，例如：<font color="red">（Linux）</font>/development/admin-manager-v1.0 或 <font color="red">（Windows）</font>E:/admin-manager-v1.0
<br/>
#### 3.4 配置管理端相关信息，打开client/config/global.js文件，找到聚合层服务器地址配置项，修改成对应的聚合层服务器地址，如下：
<br/>
```javascript
//聚合层服务器地址
GLOBAL.httpServer = "http://127.0.0.1:8001"
```
<br/>
#### 3.5 启动管理端项目，使用命令行，进入该项目目录，如下（Linux系统），windows使用dos进行相关操作，启动成功后，如下所示，表示启动完成
<br/>
```Nginx

npm run dev

> admin-manager@1.0.0 dev /Users/apple/Desktop/框架整合服务/admin-manager-v1.0
> cross-env NODE_ENV=development webpack-dev-server --config build/webpack.config.client.js

Project is running at http://0.0.0.0:8000/
webpack output is served from /

```
<br/>
#### 3.6 启动成功，打开浏览器，访问本地8000端口，例如：http://127.0.0.1:8000，会出现登录界面，如下所示，则表示管理端后台启动完成：
<br/>
<img src="/image/tutorial-8.png" width="40%" style="border:1px solid #ccc;" />
<br/><br/>
#### 3.7 登录系统，默认提供了超级管理员帐号，帐号：root 密码：123456，登录成功，进入后台，如下所示
<br/>
<img src="/image/tutorial-9.png" width="80%" style="border:1px solid #ccc;" />
<br/><br/>
### <font color="red">到此，完整安装完成，更多管理端框架的使用方式可以查看Api文档教程</font>

