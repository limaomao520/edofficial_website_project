### <font color="red">一. 原子层部署安装（Php框架），教程详见：独立部署:Hello world </font>
<br/>
#### 1.1 请先确保，完成原子层部署安装
<br/>
## <font color="red">二. 修改原子层服务框架配置文件</font>
<br/>
#### 2.1 首先对原子层的配置文件进行修改，修改部署方式配置，有中间层，如下：
<br/>
```PHP
//是否为独立部署(无聚合层)
"isIndependentDeployment" => false
```
<br/>
#### 2.2 开启后访问在原子层中访问的测试文件，地址：http://127.0.0.1/atomic-layer-php-v1.0/demo/test，如提示如下内容，则表示配置文件设置成功：
<br/>
```javascript
{
  "success" : false,
  "result"  : {
    "code"  : "00023",
    "message" : "缺少聚合层服务器编号参数，该原子层非独立部署，只可通过聚合层进行访问"
  }
}
```
<br/>
### <font color="red">三. 分层部署安装（聚合层NodeJs框架）</font>
<br/>
#### 3.1 下载聚合层框架，点击[【下载最新版本】](http://www.baidu.com)进行下载
<br/>
<img src="/image/tutorial-5.png" width="20%" style="border:1px solid #ccc;" />
<br/><br/>
#### 3.2 目录相关作用简介
<br/>
#### <font color="#666">- extension目录：放置扩展模块的接口声明文件与相关实现目录，在安装扩展模块后会将相关文件放置在本目录，用户一般无需手动修改</font>
<br/>
#### <font color="#666">- framework目录：框架核心程序目录，接口路由，各种功能类引入，缓存处理等</font>
<br/>
#### <font color="#666">- logs目录：主要用于相关的服务器日志或用户日志</font>
<br/>
#### <font color="#666">- middleBusiness目录：主要用于放置接口的配置文件和实现文件，所有需要用户具体实现的接口都放置在此处（该目录可通过配置文件自行指定名称）</font>
<br/>
#### <font color="#666">- node_modules目录：放置Nodejs的第三方模块目录，此目录为系统自带的安装目录，用户无需手动进行修改操作</font>
<br/>
#### <font color="#666">- app.js文件：服务器的启动入口文件</font>
<br/>
#### <font color="#666">- config.js文件：聚合层相关的配置信息</font>
<br/>
#### <font color="#666">- package.json文件：存放所有需要安装的三方包内容</font>
<br/>
#### 3.3 将该框架放置在安装了Nodejs的服务器的任意您需要放置的地方，例如：<font color="red">（Linux）</font>/middleServer/aggregation-layer-nodejs-v1.0 或 <font color="red">（Windows）</font>E:/aggregation-layer-nodejs-v1.0
<br/>
#### 3.4 使用命令行，进入该项目目录，如下（Linux系统），windows使用dos进行相关操作，启动成功后，如下所示，表示安装完成
<br/>
```Nginx
cd /middleServer/aggregation-layer-nodejs-v1.0  #进入项目目录

nodejs app.js                                   #使用nodejs命令启动项目的服务器入口文件，如提示如下信息则表示服务器启动成功

服务器正常启动...端口号:8001                       #默认端口号为8001，可以在配置文件中进行修改
```
<br/>
### <font color="red">四. 创建一个聚合层的测试接口Hello world</font>
<br/>
#### 4.1 打开middleBusiness/api/api.js文件，在文件中声明一个接口，内容如下所示：
<br/>
```javascript
exports.API = {

    "/demo/demo1" : {
        type   : "BUSSINESS",
        method : "GET"
    },

}
```
<br/>
#### 4.2 创建相关实现文件，在目录middleBusiness/routes下创建 <font color="red">demo</font> 文件夹，在demo文件夹下创建 <font color="red">demo1.js</font> 文件，在文件中写入如下内容：
<br/>
```javascript
exports.demo1 = function (req, res) {
    RES.successResponse(res, "hello world!!!");
}
```
<br/>
#### 4.3 访问该接口，地址为http://聚合层服务器IP 或 域名:端口号/demo/demo1，例如：http://127.0.0.1:8001/demo/demo1，在控制台中显示如下结果，表示聚合层接口创建成功，聚合层服务器已可以正常使用
<br/>
```javascript
{
  "success": true,
  "result" : "hello world!!!"
}
```
<br/>
### <font color="red">五. 聚合层访问原子层接口进行业务拼接处理模拟</font>
<br/>
#### 5.1 配置文件中配置，聚合层访问原子层的地址信息，找到如下配置信息，分别配置原子层的相关信息（请先确定原子层可以正常访问使用）
<br/>
```javascript
core_server : {
  port  : 80,                      //端口号
  name  : "atomic-layer-php-v1.0", //服务器名称
  url   : "http://127.0.0.1/"      //核心服务器地址（注意结束有/，此处不要忘记了）
}
```
<br/>
#### 5.2 在接口配置文件中，配置原子层接口信息，打开目录middleBusiness/api/core_api.js，在目录中写入需要访问的原子层接口地址，如下（这里使用之前独立部署时创建的原子层接口）：
<br/>
```javascript
exports.coreAPI = {

    //此名字可以随意进行命名，建议使用原子层接口名称的驼峰命名，如下所示
    demoTest : {
        Request_Api_Name: "/demo/test",
        Request_Api_Url: config.core_server.url + config.core_server.name + "/demo/test",
    }
   
}
```
<br/>
#### 5.3 修改之前创建聚合层接 /demo/demo1 的口内容，将原内容替换成如下，<font color="red">（注：编写完聚合层内容后，需要重启动你的服务器，修改才可生效，重启方式：直接通过ctrl + C 关闭当前启动的服务，关闭后，重新输入 node app.js 命令启动聚合层服务）</font>
<br/>
```javascript
exports.demo1 = function (req, res) {
    

    //请求聚合层，获取相关信息，R方法为封装的网络请求方式（后续会详细介绍使用方式）
    R.SEND_HTTP(
        req,
        res,
        {
            url: CORE_APIConfig.coreAPI.demoTest.Request_Api_Url, //此处为完整的原子层请求接口地址
            method: 'GET'                                         //请求方式  
        },
        function (data) {

            console.log(data);  //原子层返回值 , 结果为：hello world!!!
           
        }
    );   


}
```
#### 
<br/>
#### 该结果返回了原子层的接口信息，但是非标准的原子层数据返回结构，因为此原子层接口只是返回一个hello world!!!字符串，聚合层原样将接口返回接收到，到此就完成了一个聚合层访问原子层的模拟测试，我们可以在聚合层通过多次访问原子层接口，来拼接聚合层所需要的真实业务
<br/>
#### <font color="red" style="line-height:40px;">到此，聚合层+原子层接口框架的安装以及模拟请求完成，更多聚合层服务框架的使用方式可以查看Api文档教程</font>